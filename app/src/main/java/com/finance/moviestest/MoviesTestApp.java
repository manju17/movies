package com.finance.moviestest;

import android.app.Application;

import com.finance.moviestest.di.ApplicationComponent;
import com.finance.moviestest.di.DaggerApplicationComponent;

public class MoviesTestApp extends Application {
    ApplicationComponent applicationComponent = DaggerApplicationComponent.create();

    @Override
    public void onCreate() {
        super.onCreate();
    }

}
